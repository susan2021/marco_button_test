"""A test module for suga_simplemove."""
from pipython import GCSDevice

from suga_simplemove import move_times

with GCSDevice('C-413.1G') as pidevice:
    pidevice.ConnectTCPIP(ipaddress='10.34.50.149')
    print('initialize connected stages...')

    rangemin = pidevice.qTMN()
    rangemin = pidevice.qTMN()
    print(f'>>>>>>>> rangemin value:  {rangemin}')
    rangemax = pidevice.qTMX()
    print(f'>>>>>>>> rangemax value:  {rangemax}')
    curpos = pidevice.qPOS()
    print(f'>>>>>>>> curpos value:  {curpos}')

    move_times(pidevice=pidevice,
               number_of_moves=2,
               wait_time_between_strokes=1,
               activation_length=10,
               velocity=50)
