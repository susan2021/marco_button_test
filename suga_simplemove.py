"""The module connects to the voice coil device and return arguments."""
import time

from pipython import GCSDevice, pitools


def move_times(pidevice, number_of_moves, wait_time_between_strokes,
               activation_length, velocity):
    """Connect and set up the system and move stages and display the positions in a loop."""
    axis = pidevice.axes[0]
    pidevice.VEL(axis, velocity)
    for _ in range(number_of_moves):
        print(number_of_moves, activation_length, wait_time_between_strokes)

        time.sleep(wait_time_between_strokes)
        pidevice.MOV(axis, activation_length)
        pitools.waitontarget(pidevice, axes=axis)
        pidevice.MOV(axis, 0)

        pitools.waitontarget(pidevice, axes=axis)

        position = pidevice.qPOS(axis)[axis]
        print(f'current position of axis {axis} is {position:.2f}')
    print('done')


